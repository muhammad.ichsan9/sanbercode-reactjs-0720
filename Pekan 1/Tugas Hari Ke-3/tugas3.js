//Soal Nomor 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";
var	upper = kataKeempat.toUpperCase();
var spasi = " ";
var kapital = kataKedua.charAt(0).toUpperCase()+ 
kataKedua.slice(1);

console.log(kataPertama.concat(spasi).concat(kapital).concat(spasi).concat(kataKetiga).concat(spasi).concat(upper));

//Soal Nomor 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

var int1 = parseInt(kataPertama);
var int2 = parseInt(kataKedua);
var int3 = parseInt(kataKetiga);
var int4 = parseInt(kataKeempat);

console.log(int1+int2+int3+int4);

//Soal Nomor 3
var kalimat = "wah javascript itu keren sekali"; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama); 
console.log("Kata Kedua: " + kataKedua); 
console.log("Kata Ketiga: " + kataKetiga); 
console.log("Kata Keempat: " + kataKeempat); 
console.log("Kata Kelima: " + kataKelima);

//Soal Nomor 4
var nilai = 99;

if ( nilai >= 80){
	console.log("A");
}
else if	( nilai >= 70 && nilai <= 80){
	console.log("B");
}
else if	( nilai >= 60 && nilai <= 70){
	console.log("B");
}
else if	( nilai >= 50 && nilai <= 60){
	console.log("B");
}
else{
	console.log("E");
}


//Soal Nomor 5

var tanggal = 10;
var bulan = 2;
var tahun = 1991;
switch(bulan){
	case 1: {console.log("Januari"); break;}
	case 2: {console.log("Februari"); break;}
	case 3: {console.log("Maret"); break;}
	case 4: {console.log("April"); break;}
	case 5: {console.log("Mei"); break;}
	case 6: {console.log("Juni"); break;}
	case 7: {console.log("Juli"); break;}
	case 8: {console.log("Agustus"); break;}
	case 9: {console.log("September"); break;}
	case 10: {console.log("Oktober"); break;}
	case 11: {console.log("November"); break;}
	case 12: {console.log("Desember"); break;}
	default: {console.log("bulan");}
}

console.log(tanggal.concat(bulan).concat(tahun));